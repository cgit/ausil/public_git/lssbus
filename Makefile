CFLAGS= $(RPM_OPT_FLAGS) -Wall -D_GNU_SOURCE -g
LDFLAGS =
                                                                                            
prefix=$(DESTDIR)/usr
sbindir=$(prefix)/sbin
mandir=$(datadir)/man

all: lssbus

lssbus: lssbus.c sbus.h
	$(CC) $(CFLAGS) $(LDFLAGS) lssbus.c -o lssbus

install:
	if test -d $(sbindir); then \
		mkdir -p $(sbindir); \
	fi
	install -m 755 lssbus $(sbindir)

clean:
	rm lssbus
