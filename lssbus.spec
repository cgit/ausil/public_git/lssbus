Name: lssbus
Version: 0.1
Release: 1
Summary: Simple utility to list sbus/ebus devices
License: GPL
Group: Applications/System
URL: http://people.redhat.com/tcallawa/lssbus
Source: %name-%version.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-root
ExclusiveArch: sparc

%description
lssbus is a simple Linux/SPARC utility to list sbus/ebus devices. 
There is no rocket science here.

%prep
%setup -q

%build
CFLAGS="$RPM_OPT_FLAGS" \
make

%install
make install DESTDIR=$RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/usr/sbin/lssbus

%changelog
* Wed Apr  7 2004 Tom "spot" Callaway <tcallawa@redhat.com>
- initial rpm
