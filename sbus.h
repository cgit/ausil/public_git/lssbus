#ifndef _SBUS_H_
#define _SBUS_H_

#include <stdio.h>
                                                                                            
enum deviceClass {
        CLASS_UNSPEC, CLASS_OTHER, CLASS_NETWORK, CLASS_SCSI, CLASS_MOUSE,
        CLASS_AUDIO, CLASS_VIDEO
};
                                                                                            
enum deviceBus {
    /* 
     * BUS_SBUS is sort of a misnomer - it's more or less Sun
     * OpenPROM probing of all various associated non-PCI buses 
     */
    BUS_UNSPEC = 0,
    BUS_OTHER = (1 << 0),
    BUS_SBUS = (1 << 1),
};

struct sbusDevice {
    struct device *next;        /* next device in list */
    int index;
    enum deviceClass class;     /* type */
    enum deviceBus bus;         /* bus it's attached to */
    char * device;              /* device file associated with it */
    char * desc;                /* a description */
    /* sbus specific fields */
    struct sbusDevice *(*newDevice) (struct sbusDevice *dev );
    void (*freeDevice) (struct sbusDevice *dev);
    void (*writeDevice) (struct sbusDevice *dev);
    
    int                width;
    int               height;
    int                 freq;
    int              monitor;
};

struct sbusDevice *sbusNewDevice(struct sbusDevice *dev);
struct device *sbusProbe(enum deviceClass probeClass,
			       struct device *devlist);

struct device {
        struct device *next;
        int index;
        enum deviceClass class; /* type */
        enum deviceBus bus;     /* bus it's attached to */
        char * device;          /* device file associated with it */
        char * desc;            /* a description */
        struct device *(*newDevice) (struct device *old, struct device *new);
        void (*freeDevice) (struct sbusDevice *dev);
        void (*writeDevice) (struct device *dev);
};
                                                                                            
struct device *newDevice(struct device *old, struct device *new);
void freeDevice(struct device *dev);
void writeDevice(struct device *dev);
struct device *readDevice(FILE *file);

#endif
